package BancoPovao.estrutura.tui;

import java.util.List;

import BancoPovao.estrutura.business.BancoBO;
import BancoPovao.estrutura.domain.Banco;

public class ClienteTui {
	ContaTui get = new ContaTui();
	BancoBO bancoBO = new BancoBO();

	public void addCliente() {
		System.out.println("CADASTRAR CLIENTE");

		String nome = obterString("Informe seu nome: ");
		String cpf = obterString("Informe seu cpf: ");
		String endereco = obterString("Informe seu endere�o: ");
		String telefone = obterString("Informe seu n�mero");
		double rendaMensal = obterDouble("Informe sua renda mensal");

		Banco cliente = new Banco(nome, cpf, endereco, telefone, rendaMensal);
		try {
			bancoBO.addCliente(cliente);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void bloquearConta() {
		List<Banco> clientes = bancoBO.getAll();
		System.out.println("BLOQUEIO DE CONTA");
		Banco cliente = bancoBO.findByNome("Informe o nome do correntista", clientes);

		if(cliente == null) 
			System.out.println("Esse correntista n�o est� cadastrado.\n");
		else {
		    	
		}
		
	}

	private Double obterDouble(String string) {
		System.out.println(string);
		get.sc.nextLine();
		return get.sc.nextDouble();
	}

	private String obterString(String txt) {
		System.out.println(txt);
		return get.sc.nextLine();
	}

	private Boolean obterBoolean(String string) {
		System.out.println(string);
		switch (get.sc.nextLine().toLowerCase()) {
		case "sim":
			return true;

		case "n�o":
			return false;

		default:
			System.out.println("Informe sim ou n�o");
			return obterBoolean(string);

		}

	}

	private String converterBoolean(Boolean pergunta) {
		if (pergunta)
			return "sim";
		return "n�o";
	}
}
