package BancoPovao.estrutura.business;

import java.util.List;

import BancoPovao.estrutura.domain.Banco;
import BancoPovao.estrutura.persistence.BancoDAO;
import BancoPovao.estrutura.tui.ContaTui;

public class BancoBO {
	ContaTui get = new ContaTui();
	BancoDAO bancoDAO = new BancoDAO();
	public String addCliente(Banco cliente) {
		validar(cliente);
		return bancoDAO.addCliente(cliente);
	}
	public List<Banco> getAll(){
		return bancoDAO.getAll();
	}
	private String obterString(String txt) {
		System.out.println(txt);
		return get.sc.nextLine();
	}
	public Banco findByNome(String txt, List<Banco> clientes) {
		return bancoDAO.findByNome(txt, clientes);
	}
	private void validar(Banco cliente) {
		
		
	}
}
