package BancoPovao.estrutura.domain;

public class Banco {
	private String cpf;
	private String nome;
	private String telefone;
	private String endereco;
	private double rendaMesal;
    private String rg;
    private String comprovanteResidencia;
    private String comprovanteRenda;

	public Banco(String nome, String cpf, String endereco, String telefone, double rendaMensal) {
		this.cpf = cpf;
		this.telefone = telefone;
		this.endereco = endereco;
		this.rendaMesal = rendaMensal;

	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public double getRendaMesal() {
		return rendaMesal;
	}

	public void setRendaMesal(double rendaMesal) {
		this.rendaMesal = rendaMesal;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getComprovanteResidencia() {
		return comprovanteResidencia;
	}

	public void setComprovanteResidencia(String comprovanteResidencia) {
		this.comprovanteResidencia = comprovanteResidencia;
	}

	public String getComprovanteRenda() {
		return comprovanteRenda;
	}

	public void setComprovanteRenda(String comprovanteRenda) {
		this.comprovanteRenda = comprovanteRenda;
	}



}
