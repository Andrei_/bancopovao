package BancoPovao.estrutura.persistence;

import java.util.ArrayList;
import java.util.List;

import BancoPovao.estrutura.domain.Banco;

public class BancoDAO {

	private List<Banco> clientes = new ArrayList();

	public String addCliente(Banco cliente) {
		clientes.add(cliente);
		return null;
	}
	public void bloquearConta() {
		
	}
	public List<Banco> getAll() {
		return clientes;
	}
	public Banco findByNome(String txt, List<Banco> clientes) {
		String nome = txt;
		for (Banco cliente : clientes) {
			if(cliente.getNome().trim().equalsIgnoreCase(nome.trim())) {
				return cliente;
			}
		}
		return null;
	}

}
